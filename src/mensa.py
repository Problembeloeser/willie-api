from collections import defaultdict

import requests as requests
from bs4 import BeautifulSoup, Tag
from flask_restful import Resource

__default_location_id = 106
_URL = f'https://www.studentenwerk-leipzig.de/mensen-cafeterien/speiseplan?location={__default_location_id}'
additive_mapping = {}


class Mensa(Resource):
    def get(self):
        return parse_mensa(), 200


def __parse_mapping(section_node: Tag) -> None:
    name_mapping = {'Zusatzstoffe', 'Allergene'}
    parts = section_node.find_all('h3')
    for part in parts:
        if part.text in name_mapping:
            for row in part.findNextSibling('table').find_all('tr'):
                additive_mapping[row.find('th').text] = row.find('td').text


def __parse_section(section_node: Tag) -> dict[str, str | list[str]]:
    name = section_node.find('h4').text
    badge = [entry.text for entry in section_node.find_all('i', attrs={'class': 'meals__badge'})]
    price = str(list(section_node.find('p', attrs={'class': 'meals__price'}).children)[2]).strip().split(' / ')
    additives = {'allergents': [], 'additives': []}
    notes = []
    details = section_node.find('details')
    if details and details.find('table'):
        unparsed_additives = details.find('table').find_all('th')
        for additive_header in unparsed_additives:
            if 'Zusatzstoffe' in additive_header.text:
                additives['additives'] = additive_header.findNextSibling('td').text.split(', ')
                continue
            if 'Allergene' in additive_header.text:
                additives['allergents'] = additive_header.findNextSibling('td').text.split(', ')
                continue
        additives['additives'] = [additive_mapping[x] for x in additives['additives']]
        additives['allergents'] = [additive_mapping[x] for x in additives['allergents']]
        notes = [entry.text for entry in details.find_all('li')]
    return {'name': name, 'price': price, 'details': notes, 'badges': badge, **additives}


def parse_mensa() -> dict[str, list[dict[str, str | list[str]]]]:
    site = requests.get(_URL)
    soup = BeautifulSoup(site.content.decode('utf-8'), features='html.parser')
    __parse_mapping(soup.find('main').find('footer'))
    meal_section = soup.find('section', attrs={'class': 'meals'})
    meal_types = meal_section.find_all('h3')
    unparsed_meals = defaultdict(list)
    for meal_type in meal_types:
        node = meal_type
        while True:
            node = node.next_sibling
            if not node or node in meal_types:
                break
            if not isinstance(node, Tag) or not node.name == 'div':
                continue
            unparsed_meals[meal_type.text].append(node)
    unparsed_meals: dict[str, list[Tag]] = dict(unparsed_meals)
    meals = defaultdict(list)
    for meal, content in unparsed_meals.items():
        for sections in content:
            for choice in sections.find_all('section'):
                meals[meal].append(__parse_section(choice))
    return dict(meals)


if __name__ == '__main__':
    parse_mensa()
