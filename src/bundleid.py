import requests as requests
from flask_restful import Resource
from flask import request

__lookup_url = "https://itunes.apple.com/lookup?id={app_id}"


class BundleId(Resource):
    def get(self):
        if not (app_id := request.args.get('id')):
            return {'error_message': f'Missing ID'}, 400
        try:
            app_id = int(app_id)
        except ValueError:
            return {'error_message': f'Invalid ID {app_id}'}, 400
        result = get_bundle_id(app_id)
        if not result:
            return {}, 404
        return get_bundle_id(app_id), 200


def get_bundle_id(app_id: int) -> dict[str, str | int]:
    site = requests.get(__lookup_url.format(app_id=app_id))
    content = site.json()
    if content['resultCount'] == 0:
        return {}
    return {'app_id': app_id, 'bundle_id': content['results'][0]['bundleId']}


if __name__ == '__main__':
    get_bundle_id()
