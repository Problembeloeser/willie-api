from flask import Flask
from flask_restful import Api

from mensa import Mensa
from bundleid import BundleId


def main():
    app = Flask(__name__)
    api = Api(app)
    api.add_resource(Mensa, '/mensa')
    api.add_resource(BundleId, '/bundleid')
    app.run(host='0.0.0.0')


if __name__ == '__main__':
    main()
